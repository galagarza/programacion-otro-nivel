/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucr.ac.cr.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import ucr.ac.cr.modelo.Estudio;
import ucr.ac.cr.modelo.RegistroEstudio;
import ucr.ac.cr.vista.GUIEstudio;
import ucr.ac.cr.vista.PanelBotones;
import ucr.ac.cr.vista.PanelDatosEstudio;

/**
 *
 * @author aaron
 */
public class ControladorEstudio implements ActionListener {

    private RegistroEstudio registroEstudio;
    private PanelDatosEstudio panelDatosEstudio;
    private PanelBotones panelBotones;
    private Estudio estudio;

    public ControladorEstudio(PanelDatosEstudio panelDatosEstudio, PanelBotones panelBotones) {
        this.registroEstudio = new RegistroEstudio();
        this.panelDatosEstudio = panelDatosEstudio;
        this.panelBotones = panelBotones;
        this.estudio = null;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        /*
        *AGREGAR
         */
        if (e.getActionCommand().equalsIgnoreCase("agregar")) {
            if (this.panelDatosEstudio.getTxtID().equals("")) {
                GUIEstudio.getMensaje("Debe ingresar el ID");
            } else if (this.panelDatosEstudio.getTxtaDescripcion().equals("")) {
                GUIEstudio.getMensaje("Debe ingresar la descripción del estudio.");
            } else {
                GUIEstudio.getMensaje(this.registroEstudio.agregar(new Estudio(this.panelDatosEstudio.getTxtID(), this.panelDatosEstudio.getTxtaDescripcion())));
                this.panelDatosEstudio.limpiar();
                this.panelBotones.controlarBotones(false);
            }
        }
        /*
        *BUSCAR
         */
        if (e.getActionCommand().equalsIgnoreCase("buscar")) {
            this.estudio = (Estudio) this.registroEstudio.buscar(this.panelDatosEstudio.getTxtID());
            if (this.estudio != null) {
                this.panelDatosEstudio.setTxtaDescripcion(this.estudio.getDescripcion());
                this.panelBotones.controlarBotones(true);
            } else {
                GUIEstudio.getMensaje("El ID no se encuentra registrado");
                this.panelBotones.controlarBotones(false);
            }
        }
        /*
        *MODIFICAR
         */
        if (e.getActionCommand().equalsIgnoreCase("modificar")) {
            if (this.panelDatosEstudio.getTxtaDescripcion().equals("")) {
                 GUIEstudio.getMensaje("Debe agregar una descripción!");
            }else{
                this.estudio.setDescripcion(this.panelDatosEstudio.getTxtaDescripcion());
                GUIEstudio.getMensaje("Información modificada correctamente");
                this.panelDatosEstudio.limpiar();
                this.panelBotones.controlarBotones(false);
            }
        }
        /*
        *ELIMINAR
         */
        if (e.getActionCommand().equalsIgnoreCase("eliminar")) {
            GUIEstudio.getMensaje(this.registroEstudio.eliminar((Object)this.estudio));
            this.panelDatosEstudio.limpiar();
            this.panelBotones.controlarBotones(false);
        }
        /*
        *REPORTE
         */
        if (e.getActionCommand().equalsIgnoreCase("reporte")) {
            System.err.println("reporte");
            GUIEstudio.getMensaje(this.registroEstudio.toString());
        }
        /*
        *SALIR
         */
        if (e.getActionCommand().equalsIgnoreCase("salir")) {
            System.exit(0);
        }
    }

}
